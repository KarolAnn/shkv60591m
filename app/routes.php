<?php

namespace framework\Routing;


Router::addRoute(new \framework\Routing\Route('hello', 'HelloController@index', Route::METHOD_GET));
Router::addRoute(new \framework\Routing\Route('data', 'HelloController@data', Route::METHOD_GET));

Router::addRoute(new \framework\Routing\Route('user/{user_name}/group/{group_name}', 'HelloController@data', Route::METHOD_GET));